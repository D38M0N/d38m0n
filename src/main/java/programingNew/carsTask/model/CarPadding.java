package programingNew.carsTask.model;

public enum CarPadding {
    VELOUR("Velour", 0),
    FULLLEATHER("FullLeather",1000),
    QUILTFULLLEATHER("QuiltFullLeather", 1000);

    private String name;
    private double price;

    CarPadding(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
