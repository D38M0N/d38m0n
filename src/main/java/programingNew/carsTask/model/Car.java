package programingNew.carsTask.model;

public class Car {
    private BodyCar bodyCar;
    private FuelType fuelType;
    private CarColor carColor;
    private CarModel carModel;
    private CarPadding carPadding;
    private int walletFinishBuy;

    public int getWalletFinishBuy() {
        return walletFinishBuy;
    }

    public void setWalletFinishBuy(int walletFinishBuy) {
        this.walletFinishBuy = walletFinishBuy;
    }

    public BodyCar getBodyCar() {
        return bodyCar;
    }

    public void setBodyCar(BodyCar bodyCar) {
        this.bodyCar = bodyCar;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public CarColor getCarColor() {
        return carColor;
    }

    public void setCarColor(CarColor carColor) {
        this.carColor = carColor;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    public CarPadding getCarPadding() {
        return carPadding;
    }

    public void setCarPadding(CarPadding carPadding) {
        this.carPadding = carPadding;
    }
}
