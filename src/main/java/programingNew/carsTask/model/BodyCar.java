package programingNew.carsTask.model;

public enum BodyCar {
    SEDAN("Sedan", 0),
    PICKUP("Pick-Up", 2000),
    HATCHBACK("Hatch-Back", 1000),
    COMBI("Kombi", 1000);

    private String name;
    private double price;

    BodyCar(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
