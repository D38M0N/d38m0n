package programingNew.carsTask.model;

public enum CarModel {
    AUDI("Audi", 100000),
    BMW("BMW", 120000),
    FIAT("Fiat", 80000);

    private String name;
    private double price;

    CarModel(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

}
