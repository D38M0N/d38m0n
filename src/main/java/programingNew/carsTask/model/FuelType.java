package programingNew.carsTask.model;

public enum FuelType {
    DIESEL("Diesel", 10000),
    PETROL("Benzyna", 0),
    HYBRYD("Hybryda", 15000);

    private String name;
    private double price;

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    FuelType(String name, double price) {
        this.name = name;
        this.price = price;
    }
}
