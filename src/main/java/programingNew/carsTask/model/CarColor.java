package programingNew.carsTask.model;


public enum CarColor {
    RED("RED", 1000),
    GREEN("GREEN", 2000),
    BLUE("BLUE", 5000),
    BLACK("BLACK", 0);

    private String name;
    private double price;

    CarColor(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
