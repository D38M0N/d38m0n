package programingNew.carsTask.controler;

public interface ScannerControllerInterface {

    public int nextInt();
    public int pickOption();
}

