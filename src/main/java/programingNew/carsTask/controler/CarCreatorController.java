package programingNew.carsTask.controler;

import programingNew.carsTask.model.*;

public class CarCreatorController implements CarCreatorControllerInterface {

    private Car car;
    private ScannerControllerInterface scannerController;
    private int sizeWallet;



    public CarCreatorController(ScannerControllerInterface scannerController, int sizeWallet) {
        car = new Car();
        this.scannerController = scannerController;
        this.sizeWallet = sizeWallet;
        pickCarModel();
    }

    @Override
    public void pickCarModel() {
        int option = scannerController.pickOption();

        switch (option) {
            case 1:
                checkMoneyForCarModel(CarModel.AUDI);
                break;
            case 2:
                checkMoneyForCarModel(CarModel.BMW);
                break;

            case 3:
                checkMoneyForCarModel(CarModel.FIAT);
                break;

            default:
                pickCarModel();
                break;
        }

    }

    private void checkMoneyForCarModel(CarModel models) {
        if (sizeWallet >= models.getPrice()) {
            car.setCarModel(models);
            sizeWallet -= car.getCarModel().getPrice();
            pickCarColor();
        } else {
            pickCarModel();
        }
    }

    @Override
    public void pickCarColor() {

        int option = scannerController.pickOption();

        switch (option) {
            case 1:
                checkMoneyForColor(CarColor.BLACK);
                break;
            case 2:
                checkMoneyForColor(CarColor.RED);
                break;
            case 3:
                checkMoneyForColor(CarColor.BLUE);;
                break;
            case 4:
                checkMoneyForColor(CarColor.GREEN);;
                break;
            case 0:
                sizeWallet += car.getCarModel().getPrice();
                car.setCarModel(null);
                pickCarModel();
            default:
                pickCarColor();
                break;
        }


    }

    private void checkMoneyForColor(CarColor color) {
        if (sizeWallet >= color.getPrice()) {
            car.setCarColor(color);
            sizeWallet -= color.getPrice();
            pickBodyCar();
        } else {
            pickCarColor();
        }
    }

    @Override
    public void pickBodyCar() {

        int option = scannerController.pickOption();

        switch (option) {
            case 1:
                checkMoneyForBodyCar(BodyCar.COMBI);
                break;
            case 2:
                checkMoneyForBodyCar(BodyCar.HATCHBACK);
                break;
            case 3:
                checkMoneyForBodyCar(BodyCar.PICKUP);
                break;
            case 4:
                checkMoneyForBodyCar(BodyCar.SEDAN);
                break;
            case 0:
                sizeWallet += car.getCarColor().getPrice();
                car.setCarColor(null);
                pickCarColor();
                break;
            default:
                pickBodyCar();
                break;
        }

    }

    private void checkMoneyForBodyCar(BodyCar body) {
        if (sizeWallet >= body.getPrice()) {
            car.setBodyCar(body);
            sizeWallet -= body.getPrice();
            pickCarPadding();
        } else {
            pickBodyCar();
        }
    }

    @Override
    public void pickCarPadding() {
        int option = scannerController.pickOption();

        switch (option) {
            case 1:
                checkMoneyForCarPadding(CarPadding.FULLLEATHER);
                break;
            case 2:
                checkMoneyForCarPadding(CarPadding.QUILTFULLLEATHER);
                break;
            case 3:
                checkMoneyForCarPadding(CarPadding.VELOUR);
                break;
            case 0:
                sizeWallet += car.getBodyCar().getPrice();
                car.setBodyCar(null);
                pickBodyCar();
                break;
            default:
                pickCarPadding();
                break;
        }

    }

    private void checkMoneyForCarPadding(CarPadding padding) {
        if (sizeWallet >= padding.getPrice()) {
            car.setCarPadding(padding);
            sizeWallet -= padding.getPrice();
            pickFuelType();
        } else {
            pickCarPadding();
        }
    }

    @Override
    public void pickFuelType() {

        int option = scannerController.pickOption();

        switch (option) {
            case 1:
                checkMoneyForFuelType(FuelType.DIESEL);
                break;
            case 2:
                checkMoneyForFuelType(FuelType.PETROL);
                break;
            case 3:
                checkMoneyForFuelType(FuelType.HYBRYD);
                break;
            case 0:
                sizeWallet += car.getCarPadding().getPrice();
                car.setCarPadding(null);
                pickCarPadding();
                break;
            default:
                pickFuelType();
                break;
        }
    }

    private void checkMoneyForFuelType(FuelType fuelType) {
        if (sizeWallet >= fuelType.getPrice()) {
            car.setFuelType(fuelType);
            sizeWallet -= fuelType.getPrice();
            car.setWalletFinishBuy(sizeWallet);
        } else {
            pickFuelType();
        }
    }


    @Override
    public Car getCar() {
        return car;
    }

    @Override
    public int getSizeWallet() {
        return sizeWallet;
    }
}
