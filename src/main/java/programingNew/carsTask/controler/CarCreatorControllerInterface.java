package programingNew.carsTask.controler;

import programingNew.carsTask.model.Car;
import programingNew.carsTask.model.User;

public interface CarCreatorControllerInterface {
    public void pickCarColor();
    public void pickBodyCar();
    public void pickCarModel();
    public void pickCarPadding();
    public void pickFuelType();
    public Car getCar();
    public int getSizeWallet();

}
