package programingNew.carsTask.controler;

import java.util.Scanner;

public class ScannerController implements ScannerControllerInterface {
    @Override
    public int nextInt() {
        return new Scanner(System.in).nextInt();
    }

    @Override
    public int pickOption() {
        return new Scanner(System.in).nextInt();
    }
}
