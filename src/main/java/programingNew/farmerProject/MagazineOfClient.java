package farmerProject;


import java.util.Arrays;
import java.util.List;

public class MagazineOfClient {
    List<Magazine> magazineOfClient = Arrays.asList(
            new Magazine(0,Product.PSZENICA,1000),
            new Magazine(1,Product.ZYTO,2000),
            new Magazine(2,Product.MIESO,200),
            new Magazine(3,Product.TRUSKAWKI,300),
            new Magazine(4,Product.MARCHEWKI,150),
            new Magazine(5,Product.OGORKI,200),
            new Magazine(6,Product.ARBUZY,200),
            new Magazine(7,Product.POMIDORY,350),
            new Magazine(8,Product.SALATA,80),
            new Magazine(9,Product.KAPUSTA,160)
    );
}
