package farmerProject;

public class Magazine {
    private int idProduct;
    private Product nameOfProduct;
    private int weightProduct;

    public int getIdProduct() {
        return idProduct;
    }

    public Magazine() {
    }

    public Magazine(int idProduct, Product nameOfProduct, int weightProduct) {
        this.nameOfProduct = nameOfProduct;
        this.weightProduct = weightProduct;
        this.idProduct = idProduct;

    }

    public int getWeightProduct() {
        return weightProduct;
    }

    public void setWeightProduct(int weightProduct) {
        this.weightProduct = weightProduct;
    }

    public Product getNameOfProduct() {
        return nameOfProduct;
    }


}
