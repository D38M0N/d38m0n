package farmerProject;

public enum Product {
    PSZENICA(0, "Pszenice", 5),
    ZYTO(1, "Żyto", 4),
    MIESO(2, "Mięso", 20),
    TRUSKAWKI(3, "Truskawki", 7),
    MARCHEWKI(4, "Marchewki", 5),
    OGORKI(5, "Ogórki", 5),
    ARBUZY(6, "Arbuzy", 10),
    POMIDORY(7, "Pomidory", 5),
    SALATA(8, "Sałata", 5),
    KAPUSTA(9, "Kapusta", 9);

    private int id;
    private String name;
    private int costPerKg;


    Product(int id, String name, int costPerKg) {
        this.id = id;
        this.name = name;
        this.costPerKg = costPerKg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCostPerKg() {
        return costPerKg;
    }

    public int getId() {
        return id;
    }

    public static Product huntingProduct(int id) {
        for (Product value : Product.values()) {
            if (value.getId() == id) {
                return value;
            }
        }
        return null;
    }
}
