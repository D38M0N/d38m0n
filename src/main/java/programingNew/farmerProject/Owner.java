package farmerProject;

public class Owner {
    public String getNameOfOwner() {
        return nameOfOwner;
    }

    public void setNameOfOwner(String nameOfOwner) {
        this.nameOfOwner = nameOfOwner;
    }

    private String nameOfOwner;
    private int walletOfOwner;

    public Owner() {
    }

    public Owner(String nameOfOwner, int walletOfOwner) {
        this.nameOfOwner = nameOfOwner;
        this.walletOfOwner = walletOfOwner;
    }

    public int getWalletOfOwner() {
        return walletOfOwner;
    }

    public void setWalletOfOwner(int walletOfOwner) {
        this.walletOfOwner = walletOfOwner;
    }

}
