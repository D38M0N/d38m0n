package farmerProject;

public interface supportWholesaler {
    public void saleOfProduct();

    public void production();

    public void showWalletOwner();

    public void showMagazineProduct();

    public void closeFarm();

    public void start();

    public void createOwner();
}
   