package farmerProject;

public interface WholesalerListI {
    public Product randomProduct();
    public int randomWeight();
}
