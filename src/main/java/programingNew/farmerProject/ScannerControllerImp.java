package farmerProject;

import java.util.Scanner;

public class ScannerControllerImp  implements ScannerController{
    @Override
    public int nextInt() {
        return new Scanner(System.in).nextInt();
    }

    @Override
    public int pickOption() {
        return new Scanner(System.in).nextInt();
    }
}
