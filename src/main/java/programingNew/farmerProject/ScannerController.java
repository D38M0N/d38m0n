package farmerProject;

public interface ScannerController {
    public int nextInt();
    public int pickOption();
}
