package farmerProject;
import zadaniesalon.controller.ScannerController;

import java.util.List;
// dorobic skaner
public class SupportWholesalerImp implements supportWholesaler {
    private Owner owner = new Owner();
    private ScannerController scannerController;
    private List<Magazine> magazineOfClient = new MagazineOfClient().magazineOfClient;

    private void updateWalletOfOwner(Wholesaler productWholesaler) {
        Product product = productWholesaler.getProduct();
        int howMuchSell = productWholesaler.getHowMuch();
        int ownerWallet = owner.getWalletOfOwner();
        updateWalletOfOwner(howMuchSell, ownerWallet, product);
        takeProductWithMagazine(howMuchSell, product);
    }

    private void takeProductWithMagazine(int howMuchTake, Product product) {
        int currentlyStatusOfMagazine = magazineOfClient.get(product.getId()).getWeightProduct();
        int how = howMuchTake;
        int updateMagazineProduct = currentlyStatusOfMagazine - howMuchTake;
        magazineOfClient.get(product.getId()).setWeightProduct(updateMagazineProduct);
    }

    private void updateWalletOfOwner(int howMuchSell, int ownerWallet, Product product) {
        owner.setWalletOfOwner(ownerWallet + comteHowMuch(product, howMuchSell));
    }

    @Override
    public void saleOfProduct() {
        Wholesaler w1 = returnRandomWholesaler();
        Wholesaler w2 = returnRandomWholesaler();
        Wholesaler w3 = returnRandomWholesaler();


        int option = scannerController.pickOption();
        switch (option) {
            case 0: // refresh wholesaler
                saleOfProduct();
                break;

            case 1:
                if (checkAccessProducts(w1)) {
                } else {
                    saleOfProduct();
                }
                break;
            case 2:
                if (checkAccessProducts(w2)) {
                } else {
                    saleOfProduct();
                }
                break;
            case 3:
                if (checkAccessProducts(w3)) {
                } else {
                    saleOfProduct();
                }
                break;
            default:
                saleOfProduct();
                System.out.println("");
                break;
        }

        start();
    }

    private boolean checkAccessProducts(Wholesaler wholesaler) {
        int currentlyWeightProductOfMagazine = magazineOfClient.get(wholesaler.getProduct().getId()).getWeightProduct();
        if (wholesaler.getHowMuch() <= currentlyWeightProductOfMagazine) {
            updateWalletOfOwner(wholesaler);
            magazineOfClient.size();
            owner.getWalletOfOwner();
            return true;
        }
        return false;
    }

    private Wholesaler returnRandomWholesaler() {
        RandomWholesaler wholesaler = new RandomWholesaler();
        return new Wholesaler(wholesaler.getNameOfWholesaler(), wholesaler.getProductWhatNeed(),
                wholesaler.getHowMuchNeed());
    }

    private int comteHowMuch(Product product, int i) {
        int sum = product.getCostPerKg() * i;
        return sum;
    }

    @Override
    public void production() {
        int option = scannerController.pickOption();
        switch (option) {
            case 0:

                magazineOfClient.get(0).setWeightProduct(scannerController.pickOption());
                break;
            case 1:
                magazineOfClient.get(1).setWeightProduct(scannerController.pickOption());
                break;
            case 2:
                magazineOfClient.get(2).setWeightProduct(scannerController.pickOption());
                break;
            case 3:
                magazineOfClient.get(3).setWeightProduct(scannerController.pickOption());
                break;
            case 4:
                magazineOfClient.get(4).setWeightProduct(scannerController.pickOption());
                break;
            case 5:
                magazineOfClient.get(5).setWeightProduct(scannerController.pickOption());
                break;
            case 6:
                magazineOfClient.get(6).setWeightProduct(scannerController.pickOption());
                break;
            case 7:
                magazineOfClient.get(7).setWeightProduct(scannerController.pickOption());
                break;
            case 8:
                magazineOfClient.get(8).setWeightProduct(scannerController.pickOption());
                break;
            case 9:
                magazineOfClient.get(9).setWeightProduct(scannerController.pickOption());
                break;
            default:
                production();
                break;
        }
        start();

    }

    @Override
    public void showMagazineProduct() {
        for (Magazine magazine : magazineOfClient) {
            System.out.println(magazine.getNameOfProduct());
        }


    }

    @Override
    public void showWalletOwner() {
        owner.getWalletOfOwner();
        start();
    }

    @Override
    public void closeFarm() {
        magazineOfClient.clear();
        start();
    }

    @Override
    public void start() {
        int option = scannerController.pickOption();;
        switch (option) {
            case 1:
                showWalletOwner();
                break;
            case 2:
                production();
                break;
            case 3:
                saleOfProduct();
                break;
            case 0:
                closeFarm();
                break;

        }
    }

    @Override
    public void createOwner() {
        int option = scannerController.pickOption();
        owner.setNameOfOwner("Owner");
        owner.setWalletOfOwner(option);
        start();

    }


    public static void main(String[] args) {
        SupportWholesalerImp supportWholesalerImp = new SupportWholesalerImp();
        supportWholesalerImp.createOwner();
    }
}
