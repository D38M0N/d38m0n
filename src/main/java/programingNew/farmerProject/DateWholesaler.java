package farmerProject;

public enum DateWholesaler {
    SLONECZKO(0, "słoneczko"),
    KARTON(1, "karton"),
    INNE(2, "inne");

    private int idWholesaler;
    private String nameWholesaler;

    DateWholesaler(int idWholesaler, String nameWholesaler) {
        this.idWholesaler = idWholesaler;
        this.nameWholesaler = nameWholesaler;
    }

    public int getIdWholesaler() {
        return idWholesaler;
    }

    public String getNameWholesaler() {
        return nameWholesaler;
    }

    public static DateWholesaler huntingWholesaler(int id) {
        for (DateWholesaler value : DateWholesaler.values()) {
            if (value.getIdWholesaler() == id) {
                return value;
            }
        }
        return null;
    }
}
