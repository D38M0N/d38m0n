package farmerProject;

public class Wholesaler {
    private DateWholesaler dateWholesaler;
    private Product product;
    private int howMuch;

    public Wholesaler(DateWholesaler dateWholesaler, Product product, int howMuch) {
        this.dateWholesaler = dateWholesaler;
        this.product = product;
        this.howMuch = howMuch;
    }

    public DateWholesaler getDateWholesaler() {
        return dateWholesaler;
    }

    public Product getProduct() {
        return product;
    }

    public int getHowMuch() {
        return howMuch;
    }

    @Override
    public String toString() {
        return "Name: " + dateWholesaler +
                ", product: " + product +
                ", howMuch: " + howMuch;
    }
}
