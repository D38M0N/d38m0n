package programingNew.rentBicycle.models;

public enum ColorsOfBicycle {
    RED("Red"), BLUE("Blue"), PINK("Pink"), YELLOW("Yellow"),
    ;
    private String colors;

    ColorsOfBicycle(String colors) {
        this.colors = colors;
    }

    public String getColors() {
        return colors;
    }

    @Override
    public String toString() {
        return "ColorsOfBicycle{" +
                "colors='" + colors + '\'' +
                '}';
    }
}
