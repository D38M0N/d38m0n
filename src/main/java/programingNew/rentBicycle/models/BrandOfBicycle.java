package programingNew.rentBicycle.models;

public enum BrandOfBicycle {
    SPECIALIZED("Specialized"),
    TRECK("Trek"),
    INTENSE("Intense"),
    SCOT("Scot"),
    ROMET("Romet");

    private String nameOfBranchBicycle;

    BrandOfBicycle(String nameOfBranchBicycle) {
        this.nameOfBranchBicycle = nameOfBranchBicycle;
    }

    public String getNameOfBranchBicycle() {
        return nameOfBranchBicycle;
    }
}
