package programingNew.rentBicycle.models;

import java.time.LocalDateTime;
import java.util.Date;

public class Bicycle {
    private int numberOfModels;
    private BrandOfBicycle branchOfBicycle;
    private ColorsOfBicycle colorOfBicycle;
    private boolean statusOfBicycle;

    public void setStatusOfBicycle(boolean statusOfBicycle) {
        this.statusOfBicycle = statusOfBicycle;
    }

    private int pricePreHour;
    private LocalDateTime timeRentBicycle;

    public Bicycle(int numberOfModels, BrandOfBicycle branchOfBicycle, ColorsOfBicycle colorOfBicycle, boolean statusOfBicycle, int pricePreHour) {
        this.numberOfModels = numberOfModels;
        this.branchOfBicycle = branchOfBicycle;
        this.colorOfBicycle = colorOfBicycle;
        this.statusOfBicycle = statusOfBicycle;
        this.pricePreHour = pricePreHour;
    }

    public LocalDateTime getTimeRentBicycle() {
        return timeRentBicycle;
    }

    public void setTimeRentBicycle( LocalDateTime timeRentBicycle) {
        this.timeRentBicycle = timeRentBicycle;
    }

    public int getNumberOfModels() {
        return numberOfModels;
    }

    public BrandOfBicycle getBranchOfBicycle() {
        return branchOfBicycle;
    }

    public ColorsOfBicycle getColorOfBicycle() {
        return colorOfBicycle;
    }
    public boolean getStatusOfBicycle(){
        return statusOfBicycle;
    }

    public String isStatusOfBicycle() {
        if(statusOfBicycle){
            return "Open";
        }
        return "Close";
    }

    public int getPricePreHour() {
        return pricePreHour;
    }

    @Override
    public String toString() {
        return "Bicycle{" +
                "numberOfModels " + numberOfModels +
                ", branchOfBicycle " + branchOfBicycle +
                ", colorOfBicycle " + colorOfBicycle +
                ", statusOfBicycle " + isStatusOfBicycle() +
                ", pricePreHour " + pricePreHour +
               '}';
    }
}
