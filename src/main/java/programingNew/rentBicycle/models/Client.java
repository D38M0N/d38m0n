package programingNew.rentBicycle.models;

public class Client {
    private String name;

    public void setSizeWallet(double sizeWallet) {
        this.sizeWallet = sizeWallet;
    }

    private double sizeWallet;

    public String getName() {
        return name;
    }

    public double getSizeWallet() {
        return sizeWallet;
    }

    public Client(String name, double sizeWallet) {
        this.name = name;
        this.sizeWallet = sizeWallet;
    }
}
