package programingNew.rentBicycle.controlers;

import programingNew.rentBicycle.models.Bicycle;
import programingNew.rentBicycle.models.BrandOfBicycle;
import programingNew.rentBicycle.models.Client;
import programingNew.rentBicycle.models.ColorsOfBicycle;

import java.time.LocalDateTime;
import java.util.List;


public class RentController implements RentControllerInterface {

    private List<Bicycle> availableBicycle = new BicycleGarage().dateBase();
    private ScannerControllerInterface scannerControllerInterface = new ScannerController();
    private Client client;


    @Override

    public void showDateBaseModelOfBicycle() {
        for (Bicycle bicycle : availableBicycle) {

            System.out.println(bicycle);
        }

        chooseFilter();
    }

    @Override
    public void createProfilClient() {
        String name =scannerControllerInterface.setName();
        int wallet = scannerControllerInterface.setWallet();
         client = new Client (name,wallet);
        showDateBaseModelOfBicycle();


    }

    @Override
    public void chooseFilter() {

        int choose = scannerControllerInterface.pickOption();
        switch (choose) {

            case 1:
                filterDateBaseByBrand();
                break;
            case 2:
                filterDateBaseByColor();
                break;
            case 3:
                filterDateBaseByPricePreHour();
                break;
            case 4:
                filterDateBaseByStatus();
                break;
            default:
                chooseFilter();
                break;
        }


    }

    @Override
    public void filterDateBaseByColor() {
        int choose = scannerControllerInterface.pickOption();
        switch (choose) {
            case 1://red
                selectAndShowColors(ColorsOfBicycle.RED);
                break;
            case 2:
                selectAndShowColors(ColorsOfBicycle.BLUE);
                break;
            case 3:
                selectAndShowColors(ColorsOfBicycle.YELLOW);
                break;
            case 4:
                selectAndShowColors(ColorsOfBicycle.PINK);
                break;
            default:
                chooseFilter();
                break;

        }

    }

    private void selectAndShowColors(ColorsOfBicycle colorsOfBicycle) {
        for (Bicycle bicycle : availableBicycle) {
            if (bicycle.getColorOfBicycle() == colorsOfBicycle) {
                System.out.println(bicycle);
            }
        }
        for (Bicycle bicycle : availableBicycle) {
            if (bicycle.getColorOfBicycle() != colorsOfBicycle) {
                System.out.println(bicycle);
            }
        }
        pickRentModelOfBicycle();
    }

    @Override
    public void filterDateBaseByBrand() {
        int choose = scannerControllerInterface.pickOption();
        switch (choose) {
            case 1:
                selectAndShowBrand(BrandOfBicycle.INTENSE);
                break;
            case 2:
                selectAndShowBrand(BrandOfBicycle.SCOT);
                break;
            case 3:
                selectAndShowBrand(BrandOfBicycle.SPECIALIZED);
                break;
            case 4:
                selectAndShowBrand(BrandOfBicycle.TRECK);
                break;
            case 5:
                selectAndShowBrand(BrandOfBicycle.ROMET);
                break;
            default:
                chooseFilter();
                break;
        }
    }

    private void selectAndShowBrand(BrandOfBicycle brandOfBicycle) {
        for (Bicycle bicycle : availableBicycle) {
            if (bicycle.getBranchOfBicycle() == brandOfBicycle) {
                System.out.println(bicycle);
            }
        }
        for (Bicycle bicycle : availableBicycle) {
            if (bicycle.getBranchOfBicycle() != brandOfBicycle) {
                System.out.println(bicycle);
            }
        }
        pickRentModelOfBicycle();
    }

    @Override
    public void filterDateBaseByStatus() {
        int choose = scannerControllerInterface.pickOption();
        switch (choose) {
            case 1:
                selectAndShowStatusTrue();
                break;
            case 2:
                selectAndShowStatusFalse();
                break;
            default:
                filterDateBaseByStatus();
        }


    }

    private void selectAndShowStatusTrue() {
        for (Bicycle bicycle : availableBicycle) {
            if (bicycle.getStatusOfBicycle()) {
                System.out.println(bicycle);
            }
        }
        for (Bicycle bicycle : availableBicycle) {
            if (bicycle.getStatusOfBicycle()) {
            } else {
                System.out.println(bicycle);
            }
        }
        pickRentModelOfBicycle();
    }

    private void selectAndShowStatusFalse() {
        for (Bicycle bicycle : availableBicycle) {
            if (bicycle.getStatusOfBicycle()) {
            } else {
                System.out.println(bicycle);
            }
        }
        for (Bicycle bicycle : availableBicycle) {
            if (bicycle.getStatusOfBicycle()) {
                System.out.println(bicycle);
            }
        }

        pickRentModelOfBicycle();
    }

    @Override
    public void filterDateBaseByPricePreHour() {
        int choose = scannerControllerInterface.pickOption();
        for (Bicycle bicycle : availableBicycle) {
            if (bicycle.getPricePreHour() >= choose) {
                System.out.println(bicycle);
            }
        }
        pickRentModelOfBicycle();
    }

    @Override
    public void checkWalletAfterRent() {
        int choose = scannerControllerInterface.pickOption();
        switch (choose) {
            case 1:
                client.setSizeWallet(client.getSizeWallet()+scannerControllerInterface.setWallet());
                break;
            case 2:
                chooseWhatDo();
                break;
            case 3:
                if(client.getSizeWallet()>0){
                    break;
                }else{
                    System.out.println("Your wallet is minus. Add your Wallet!");
                    checkWalletAfterRent();
                }
            default:
                checkWalletAfterRent();
        }

    }

    @Override
    public void pickRentModelOfBicycle() {
        int choose = scannerControllerInterface.pickOption();
        for (Bicycle bicycle : availableBicycle) {

            if (bicycle.getNumberOfModels() == choose) {
                bicycle.setStatusOfBicycle(false);
                bicycle.setTimeRentBicycle(LocalDateTime.now());

            }
        }
        showRentModelBicycle();
        chooseWhatDo();

    }

    @Override
    public void chooseWhatDo() {
        int choose = scannerControllerInterface.pickOption();
        switch (choose) {
            case 1:
                showDateBaseModelOfBicycle();
                break;
            case 2:
                pickReturnRentModelOfBicycle();
                break;
            case 3:
                checkWalletAfterRent();
                break;
            default:
                chooseWhatDo();
                break;
        }
    }

    @Override
    public void pickReturnRentModelOfBicycle() {
        showRentModelBicycle();
        long costRentBicycle = 0;
        int choose = scannerControllerInterface.pickOption();
        for (Bicycle bicycle : availableBicycle) {

            if (bicycle.getNumberOfModels() == choose && bicycle.getTimeRentBicycle() != null) {
                bicycle.setStatusOfBicycle(true);
                costRentBicycle = counterOfCostRentBicycle(bicycle);
                client.setSizeWallet(client.getSizeWallet() - costRentBicycle);

            }
        }
        System.out.println(costRentBicycle+ " " + client.getSizeWallet());

        chooseWhatDo();

    }

    private long counterOfCostRentBicycle(Bicycle bicycle) {
        long timeOfRent = java.time.Duration.between(bicycle.getTimeRentBicycle(), LocalDateTime.now()).toSeconds();
        long costRentBicycle = timeOfRent * bicycle.getPricePreHour();
        bicycle.setTimeRentBicycle(null);
        return costRentBicycle;
    }

    private void showRentModelBicycle() {
        for (Bicycle bicycle : availableBicycle) {
            if (bicycle.getStatusOfBicycle() == false) {
                System.out.println(bicycle);
            }
        }
    }


    @Override
    public Bicycle getBicycle() {
        return null;
    }
}
