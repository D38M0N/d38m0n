package programingNew.rentBicycle.controlers;

import programingNew.rentBicycle.models.Bicycle;
import programingNew.rentBicycle.models.BrandOfBicycle;
import programingNew.rentBicycle.models.ColorsOfBicycle;

import java.util.Arrays;
import java.util.List;

public class BicycleGarage {
    public List<Bicycle> dateBase() {
        List<Bicycle> dateBase = Arrays.asList(

                new Bicycle(1, BrandOfBicycle.INTENSE, ColorsOfBicycle.BLUE, true, 4),
                new Bicycle(2, BrandOfBicycle.INTENSE, ColorsOfBicycle.RED, true, 2),
                new Bicycle(3, BrandOfBicycle.SPECIALIZED, ColorsOfBicycle.BLUE, true, 2),
                new Bicycle(4, BrandOfBicycle.SPECIALIZED, ColorsOfBicycle.RED, true, 6),
                new Bicycle(5, BrandOfBicycle.ROMET, ColorsOfBicycle.BLUE, true, 2),
                new Bicycle(6, BrandOfBicycle.ROMET, ColorsOfBicycle.PINK, true, 7),
                new Bicycle(7, BrandOfBicycle.TRECK, ColorsOfBicycle.YELLOW, true, 5),
                new Bicycle(8, BrandOfBicycle.TRECK, ColorsOfBicycle.YELLOW, true, 6),
                new Bicycle(9, BrandOfBicycle.SCOT, ColorsOfBicycle.RED, true, 4),
                new Bicycle(10, BrandOfBicycle.SCOT, ColorsOfBicycle.PINK, true, 9));

        return dateBase;
    }
}
