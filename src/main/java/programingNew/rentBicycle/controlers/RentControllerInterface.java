package programingNew.rentBicycle.controlers;

import programingNew.rentBicycle.models.Bicycle;

import java.util.List;

public interface RentControllerInterface {

    public void pickRentModelOfBicycle();
    public void pickReturnRentModelOfBicycle();
    public void showDateBaseModelOfBicycle();
    public void chooseFilter();
    public void filterDateBaseByColor();
    public void filterDateBaseByBrand();
    public void filterDateBaseByStatus();
    public void filterDateBaseByPricePreHour();
    public void chooseWhatDo();
    public void createProfilClient();
    public void checkWalletAfterRent();
    public Bicycle getBicycle();
}
