package programingNew.rentBicycle.controlers;

public interface ScannerControllerInterface {
    public int pickOption();
    public String setName();
    public int setWallet();

}
