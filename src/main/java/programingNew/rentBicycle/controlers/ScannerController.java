package programingNew.rentBicycle.controlers;

import java.util.Scanner;

public class ScannerController implements ScannerControllerInterface {
    @Override
    public int pickOption() {
        return new Scanner(System.in).nextInt();
    }

    @Override
    public String setName() {
        return new Scanner(System.in).next();
    }

    @Override
    public int setWallet() {
        return new Scanner(System.in).nextInt();
    }
}
