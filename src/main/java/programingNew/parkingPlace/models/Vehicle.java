package parkingPlace.models;

import java.time.LocalDateTime;

public class Vehicle {
    private ModelVehicle modelVehicle;
    private boolean isFree;
    private LocalDateTime date;
    private int position;

    public Vehicle() {
    }

    public Vehicle(ModelVehicle modelVehicle, boolean isFree, LocalDateTime date, int position) {
        this.date = date;
        this.modelVehicle = modelVehicle;
        this.isFree = isFree;
        this.position = position;
    }

    public ModelVehicle getModelVehicle() {
        return modelVehicle;
    }

    public boolean isFree() {
        return isFree;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public int getPosition() {
        return position;
    }

    public void setModelVehicle(ModelVehicle modelVehicle) {
        this.modelVehicle = modelVehicle;
    }

    public void setFree(boolean free) {
        isFree = free;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return                "modelVehicle: " + modelVehicle +
                ", isFree: " + isFree +
                ", date: " + date +
                ", position: " + position ;
    }
}
