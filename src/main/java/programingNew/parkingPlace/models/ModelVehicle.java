package parkingPlace.models;

public enum ModelVehicle {
    DELIVER_CAR("Deliver car",15),
    PASSENGER_CAR("Passenger car",10),
    MOTORCYCLE("Motorcycle",5);
    private String name;
    private int payPerHour;

    ModelVehicle(String name, int payByHour) {
        this.name = name;
        this.payPerHour = payByHour;
    }

    public String getName() {
        return name;
    }

    public int getPayPerHour() {
        return payPerHour;
    }
}
