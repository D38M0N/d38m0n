package parkingPlace.controller;


import parkingPlace.models.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class ParkControl {
    List<Vehicle> vehiclesOnPark = new ArrayList<>();
    ChoseVehicle choseVehicle = new ChoseVehicleImp();
    int payForAllParking = 0;
    int placesOnPark = 5;

    public void createParking() {
        for (int i = 1; i <= placesOnPark; i++) {
            vehiclesOnPark.add(new Vehicle(null, true, null, i));
        }
        start();
    }

    private void start() {
        int option = choseVehicle.loadNumber();
        switch (option) {
            case 1: // park vehicle
                if (isBusy()) {
                    start();
                } else {
                    addVehicleToPark();
                }
                break;
            case 2: //  out vehicle
                deleteVehicleWithPark();
                break;
            case 3: // pay by all  parking
                System.out.println(payForAllParking);
                start();
                break;
            case 4:// show free place
                freePlaceOnParking();
                break;
            default:
                start();
        }
    }

    private void freePlaceOnParking() {
        for (Vehicle vehicle : vehiclesOnPark) {
            if (vehicle.isFree()) {
                System.out.println(vehicle);
            }
        }
        start();
    }

    private void addVehicleToPark() {
        showCurrentlyStatusPark();
        System.out.println("Choose your vehicle model.");
        int option = choseVehicle.loadNumber();
        switch (option) {
            case 1:
                parkUpDeliverCar();
                start();
                break;
            case 2:
                parkUpPassengerCar();
                start();
                break;
            case 3:
                parkUpMotorcycle();
                start();
                break;
        }
    }

    private void parkUpDeliverCar() {
        for (Vehicle vehicle : vehiclesOnPark) {

            if (vehicle.isFree()) {
                vehicle.setDate(LocalDateTime.now());
                vehicle.setFree(false);
                vehicle.setModelVehicle(ModelVehicle.DELIVER_CAR);
                start();
            }
        }
    }

    private void parkUpMotorcycle() {
        for (Vehicle vehicle : vehiclesOnPark) {
            if (vehicle.isFree()) {
                vehicle.setDate(LocalDateTime.now());
                vehicle.setFree(false);
                vehicle.setModelVehicle(ModelVehicle.MOTORCYCLE);
                start();
            }
        }
    }

    private void parkUpPassengerCar() {
        for (Vehicle vehicle : vehiclesOnPark) {
            if (vehicle.isFree()) {
                vehicle.setDate(LocalDateTime.now());
                vehicle.setFree(false);
                vehicle.setModelVehicle(ModelVehicle.PASSENGER_CAR);
                start();
            }
        }
    }

    private void showCurrentlyStatusPark() {
        for (Vehicle vehicle : vehiclesOnPark) {

        }
    }

    private boolean isBusy() {
        boolean isBusy = true;
        for (Vehicle vehicle : vehiclesOnPark) {
            if (vehicle.isFree()) {
                isBusy = false;
            }
        }
        return isBusy;
    }

    private void deleteVehicleWithPark() {
        showCurrentlyStatusPark();
        int option = choseVehicle.loadNumber();
        int parkTime = 0, payForParking = 0;

        for (Vehicle vehicle : vehiclesOnPark) {
            if (vehicle.isFree() == false && vehicle.getPosition() == option) {
                parkTime = Duration.between(vehicle.getDate(), LocalDateTime.now()).toSecondsPart();
                payForParking = vehicle.getModelVehicle().getPayPerHour() * parkTime;
                vehicle.setDate(null);
                vehicle.setFree(true);
                vehicle.setModelVehicle(null);
                payForAllParking += payForParking;
                start();
            }
        }
        start();
    }

}

