package programingNew.writingLOVE;

public interface CreatePrinter {
    public void spacePrint(int howMuchReturn);
    public void horizontalPrinter();
    public void verticalPrinter(int howMuchReturn);
}
