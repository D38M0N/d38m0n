package programingNew.writingLOVE;

public class makeLiter implements CreatePrinter, CreateLiters {
    @Override
    public void printL(int height) {
        for (int i = 0; i < height - 1; i++) {
            horizontalPrinter();
        }
        verticalPrinter(height - 1);
        System.out.println("\n");

    }


    @Override
    public void printO(int height) {
        for (int i = 0; i < height - 1; i++) {
            if (i == 0) {
                verticalPrinter(height - 1);
                horizontalPrinter();
            }
            if (i > 0 & i < height) {
                bodyLineO(height - 2);
            }
            if (i == height - 2) {
                verticalPrinter(height - 1);
                horizontalPrinter();
            }

        }
        System.out.print("\n");
    }


    private void bodyLineO(int height) {
        verticalPrinter(1);
        for (int i = 0; i < height; i++) {
            System.out.print("  ");
        }
        horizontalPrinter();

    }

    @Override
    public void printV(int height) {
        for (int i = 0; i < height; i++) {
            spacePrint(i);
            System.out.print("*");
            spacePrint((height - 1 - i) * 2);
            System.out.println("*");

        }
        System.out.print("\n");
    }

    @Override
    public void printE(int height) {

        verticalPrinter(height - 3);
        for (int i = 0; i < height / 2; i++) {
            horizontalPrinter();
        }
        verticalPrinter(height - 3);

        for (int i = 0; i < height / 2; i++) {
            horizontalPrinter();
        }
        verticalPrinter(height - 2);
        System.out.println("");

    }

    @Override
    public void spacePrint(int howMuchReturn) {
        for (int i = 0; i < howMuchReturn; i++) {
            System.out.print(" ");
        }
    }

    @Override
    public void horizontalPrinter() {
        System.out.println("*");

    }

    @Override
    public void verticalPrinter(int howMuchReturn) {
        for (int i = 0; i < howMuchReturn; i++) {
            System.out.print("* ");
        }
    }

    public void createLOVE(int height) {
        printL(height);
        printO(height);
        printV(height);
        printE(height);
    }

    public static void main(String[] args) {
        makeLiter me = new makeLiter();
        int nums = 7;
        me.createLOVE(nums);

    }
}
